compilar:limpiar
	mkdir bin
	find src -name *.java | xargs javac -cp bin -d bin
limpiar:
	rm -rf bin
ejecutar:compilar
	java -cp bin aplicacion.Principal
jar:compilar
	jar cvfm ap-personas.jar manifest.txt -c bin .
