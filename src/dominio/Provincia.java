package dominio;

import java.util.ArrayList;

public class Provincia{
	private String nombre;
	public ArrayList<Municipio> municipios = new ArrayList<>();
	private int numeroDeHabitantes = 0;


	public String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public int calcularNumeroHabitantes(){
		for(Municipio muns : municipios ){
                	numeroDeHabitantes = numeroDeHabitantes + muns.calcularNumeroHabitantes();
		}
		return numeroDeHabitantes;
	}

	public String toString(){
		return "Provincia: " + nombre + " \nNumero de habitantes: " + calcularNumeroHabitantes();
	}
}
