package dominio;

import dominio.Localidad;
import java.util.ArrayList;

public class Municipio{
	private String nombre;
	public ArrayList<Localidad> localidades = new ArrayList<>();
	private int numeroDeHabitantes = 0;

	public String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public int calcularNumeroHabitantes(){
		for(Localidad locs : localidades ){
                numeroDeHabitantes = numeroDeHabitantes + locs.getNumeroDeHabitantes();
		}
		return numeroDeHabitantes;
	}

	public String toString(){
		return nombre + " Numero de habitantes: " + calcularNumeroHabitantes();
	}
}
