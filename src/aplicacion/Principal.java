package aplicacion;

import dominio.*;
import java.util.ArrayList;

public class Principal{
	public static void main(String[] args){
	
		Provincia prov1 = new Provincia();
        prov1.setNombre("Madrid");

		Municipio mun1 = new Municipio();
		mun1.setNombre("Fuenlabrada");

		Municipio mun2 = new Municipio();
		mun2.setNombre("Colmenarejo");



        Localidad loc1 = new Localidad();
        loc1.setNombre("localidad1");
		loc1.setNumeroDeHabitantes(6000);
		mun1.localidades.add(loc1);

        Localidad loc2 = new Localidad();
        loc2.setNombre("localidad2");
		loc2.setNumeroDeHabitantes(1000);
		mun2.localidades.add(loc2);

        Localidad loc3 = new Localidad();
        loc3.setNombre("localidad4");
		loc3.setNumeroDeHabitantes(30000);
        mun1.localidades.add(loc3);

        Localidad loc4 = new Localidad();
        loc4.setNombre("localidad4");
		loc4.setNumeroDeHabitantes(15000);
		mun2.localidades.add(loc4);
        
        Localidad loc5 = new Localidad();
        loc5.setNombre("localidad5");
		loc5.setNumeroDeHabitantes(20000);
		mun1.localidades.add(loc5);
        
        Localidad loc6 = new Localidad();
		loc6.setNombre("localidad6");
		loc6.setNumeroDeHabitantes(10000);
		mun2.localidades.add(loc6);

		prov1.municipios.add(mun1);
        prov1.municipios.add(mun2);

		System.out.println(prov1.toString());
	}
}
